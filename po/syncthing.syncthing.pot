# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the syncthing.syncthing package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: syncthing.syncthing\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-10-01 16:26+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/Main.qml:103
msgid "Sign in"
msgstr ""

#: ../qml/Main.qml:118
msgid "username"
msgstr ""

#: ../qml/Main.qml:123
msgid "password"
msgstr ""

#: ../qml/Main.qml:128
msgid "Sign In"
msgstr ""

#: ../qml/Main.qml:139
msgid "Cancel"
msgstr ""

#: syncthing.desktop.in.h:1
msgid "Syncthing"
msgstr ""
