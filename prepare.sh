#!/bin/bash
set -e

VERSION=$(echo $(cat ${ROOT}/RELEASE || git describe --always) | sed -e "s/^v//" | sed -r "s/-[0-9]{1,3}-g[0-9a-f]{5,10}//") # try RELEASE file, then git description

IFS='-' read -ra ARR<<<"${ARCH_TRIPLET}"
ARCH=$(echo ${ARR[0]}|sed -e "s/armhf/arm/" -e "s/aarch/arm/" -e "s/x86_/amd/")
SOURCE="syncthing-linux-${ARCH}-v${VERSION}"

CACHE=${ROOT}/cache

echo "Downloading Syncthing Web ..."
echo "VERSION: ${VERSION}"
echo "ARCH   : ${ARCH}"
echo "SOURCE : ${SOURCE}.tar.gz"
echo "CACHE  : ${CACHE}"
echo "-----------------------"

#mkdir -p ${INSTALL_DIR}/bin
mkdir -p ${CACHE}

[ ! -f "${CACHE}/${SOURCE}/syncthing" ] && wget -nv -O- https://github.com/syncthing/syncthing/releases/download/v${VERSION}/${SOURCE}.tar.gz | tar -xzvC ${CACHE}
([ ! -f "${BUILD_DIR}/syncthing-web" ] && cp ${CACHE}/${SOURCE}/syncthing ${BUILD_DIR}/syncthing-web) || true
