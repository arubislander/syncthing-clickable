#ifndef SYNCTHINGHELPER_H
#define SYNCTHINGHELPER_H

#include <QObject>
#include <QProcess>

class SyncthingHelper: public QObject {
    Q_OBJECT

public:
    SyncthingHelper();
    ~SyncthingHelper();

    Q_INVOKABLE void start();
    Q_INVOKABLE void stop();
    Q_INVOKABLE bool migrate();

private slots:
    void processStarted();

signals:
    void started();

private:
    QProcess m_process;
    QString m_configPath;
    QString m_homePath;
};

#endif