#include <QDebug>
#include <QProcess>
#include <QThread>
#include <QDir>
#include <QFileInfo>
#include <QFile>
#include <QStandardPaths>

#include "syncthinghelper.h"

SyncthingHelper::SyncthingHelper() {
    m_configPath = QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation);
    qDebug() << "<--- configuration path: " << m_configPath;

    m_homePath = "/home/phablet"; //env.value("HOME");
    qDebug() << "<--- $HOME: " << m_homePath;
}

SyncthingHelper::~SyncthingHelper() {
    stop();
}

bool SyncthingHelper::migrate() {
    qDebug() << "<--- clean up old service";

    QDir upstartDir(QString("%1/.config/upstart").arg(m_homePath));
    upstartDir.setNameFilters(QStringList() << "syncthing*.conf");
    QFileInfoList list = upstartDir.entryInfoList();
    for (int i=0; i < list.size(); i++) {
        QFileInfo info = list.at(i);
        QFile::remove(info.filePath());
        qDebug() << "<--- removed " << info.filePath();
    }

    qDebug() << "<--- move config";
    // move /home/phablet/syncthing to app config folder
    QDir configDir(m_configPath);
    if (!configDir.exists()) configDir.mkpath(".");
    QDir syncthingDir(QString("%1/.config/syncthing").arg(m_homePath));
    list = syncthingDir.entryInfoList();

    // Move files to new location
    for (int i=0; i < list.size(); i++) {
        QFileInfo info = list.at(i);
        syncthingDir.rename(info.fileName(), configDir.absoluteFilePath(info.fileName()));
        qDebug() << "<--- moved " << info.fileName() << " to " << configDir.absoluteFilePath(info.fileName());
    }

    syncthingDir.refresh();
    if (syncthingDir.isEmpty()) {
        syncthingDir.removeRecursively();
        return true;
    }
    return false;
}

void SyncthingHelper::start() {
    qDebug() << "<--- starting webapp";

    QObject::connect( &m_process, &QProcess::started, this, &SyncthingHelper::processStarted);

    QStringList args;
    args << "serve" 
         << "--gui-address=127.0.0.1:8384"
         << "--no-upgrade"
         << "--no-browser"
         << "--no-restart"
         << "--logflags=0"
         << QString("--home=%1").arg(m_configPath);

    qDebug() << "syncthing-web " << args;

    // ExecStart=/opt/click.ubuntu.com/syncthing.syncthing/current/syncthing-web serve --gui-address=127.0.0.1:8384 --no-upgrade --no-browser --no-restart --logflags=0
    m_process.start("syncthing-web", args);
}

void SyncthingHelper::stop() {
    qDebug() << "<--- stopping webapp";
    m_process.terminate();
}

void SyncthingHelper::processStarted() {
    qDebug() << "<--- emitting started()";
    QThread::sleep(5);
    emit started();
    QObject::disconnect( &m_process, &QProcess::started, this, &SyncthingHelper::processStarted);
}
