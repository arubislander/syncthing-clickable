#include <QtQml>
#include <QtQml/QQmlContext>

#include "plugin.h"
#include "syncthinghelper.h"

void SyncthingHelperPlugin::registerTypes(const char *uri) {
    //@uri Example
    qmlRegisterSingletonType<SyncthingHelper>(uri, 1, 0, "SyncthingHelper", [](QQmlEngine*, QJSEngine*) -> QObject* { return new SyncthingHelper; });
}
